const main = document.querySelector(".main");
const section = document.createElement("section");

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  main.addEventListener("click", () => {
    section.innerHTML = `
      <article class="message">Lorem ipsum, dolor sit amet!</article>
      <article class="message">Nihil, quibusdam! Esse reprehenderit.</article>
      <article class="message">Quis perspiciatis cum exercitationem.</article>
      <article class="message">Earum adipisci at voluptates veritatis?</article>
      <article class="message">Accusantium magnam iusto saepe fuga!</article>
  `;
    main.appendChild(section);
  });
});
